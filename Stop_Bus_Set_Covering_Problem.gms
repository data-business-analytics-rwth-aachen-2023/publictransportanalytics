Sets

i set of passenger demand points /1*9/
j set of possible transport stop loaction /A*C/

;

Parameters
d(i,j) distances from i to j
A(i,j) binary matrix. 1if i is covered by stop location j. 0 otherwise
r max distance /300/;

Table
d(i,j) distances from i to j
     A       B       C
1    714.56  419.66  300.00
2    651.04  403.21  297.16
3    514.37  345.37  288.66
4    327.79  258.03  277.32
5    160.49  170.13  268.81
6    240.45  155.39  267.67
7    434.40  233.08  273.91
8    597.73  324.95  285.26
9    695.84  391.87  294.90
;

A(i,j)$(d(i,j)<=r)=1;

free variable
G;
Binary Variable
X(j);

Equations
objectiveF
Cover;


objectiveF.. G=e=sum(j,x(j));
Cover(i)..sum(j$(A(i,j)>0),x(j))=g=1;

model SCP /all/;
solve SCP min G using mip;

Display x.l;




