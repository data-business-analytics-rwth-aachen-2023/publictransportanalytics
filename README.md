# Tutorial 1: Overview
(GAMS code "shortestPath.gms")

**As you approach the forthcoming challenges, consider the following key questions:**
- Is the graph directed or undirected?
- Do the graph's edges carry weights?
- Which is more appropriate for this situation: an adjacency matrix, an incidence matrix, or an adjacency list?

## Shortest Path Problem

![Shortest Path Diagram](ShortestPath.PNG)


**Sets:**

- $I$: set of nodes, $ I = \{ A, B, C, D, E, F, G, H \} $. Represented by $i$, $j$
- $E$: set of arcs, $(i,j)$ where $i, j \in I$

**Parameter:**

- $D(i,j)$: distance from node $i$ to node $j$

|     | A | B | C | D | E | F | G | H |
|-----|---|---|---|---|---|---|---|---|
| A   | - | 3 | 6 | - | - | - | - | - |
| B   | - | - | 4 | 4 | 11| - | - | - |
| C   | - | - | - | 3 | - | - | - | 11|
| D   | - | - | - | - | 5 | 5 | 2 | - |
| E   | - | - | - | - | - | - | - | 9 |
| F   | - | - | - | - | - | - | - | 9 |
| G   | - | - | - | - | - | - | - | 2 |
| H   | - | - | - | - | - | - | - | - |


**Variables:**

- $X(i,j)$: 1 if visiting node $j$ after node $i$, 0 otherwise.
- $Z$: value of the objective function.

**Equations:**

1. Objective Function:

Min $Z = \sum_{(i,j) \in E} D(i,j) \cdot X(i,j)$

2. Origin:

$\sum_{i \in E(i, 'H')} X(i, 'H') = 1$

3. Destination:

$\sum_{j \in E('A', j)} X('A', j) = 1$

4. Flow:

$\sum_{i \in E(i,j)} X(i, j) - \sum_{i \in E(j,i)} X(j,i) = 0$

4. Domain:
$X(i, j) \in \{0,1\}$     $\forall i, j \in I$


# Tutorial 2: Bus stop Location
Model 1: "Stop_Bus_Set_Covering_Problem.gms"