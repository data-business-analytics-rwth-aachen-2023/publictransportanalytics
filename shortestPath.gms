* Shortest path problem

Set
I set of nodes /A*H/
E(I,I) set of arcs /(A*H).(A*H)/;

alias(i,j);

Parameter
D(i,j) distance from i to j
;

Table
D(i,j)
    A   B   C   D   E   F   G   H
A       3   6                   
B           4   4   11          
C               3           11  
D                   5   5   2   
E                               9
F                               9
G                               2
H                               
;

* Removing arcs from the set E
E(i,j)$(D(i,j)=0)=no;


Binary variable
X(i,j) 1 if I am visiting j after visit i 0 otherwhise;

Free Variable
Z the value of the objective function;


Equations objectiveFunction;
objectiveFunction.. Z=e=sum((i,j)$(E(i,j)),D(i,j)*X(i,j));

Equation origen;
origen..sum(i$(E(i,'H')),X(i,'H'))=e=1;

Equation Destination;
Destination..sum(j$(E('A',j)),X('A',j))=e=1;

Equation flow;
flow(j)$(ord(j)>1 and ord(j)<card(I))..sum(i$(E(i,j)),X(i,j))-sum(i$(E(j,i)),X(j,i))=e=0;

Model  sp /all/;
Solve sp min z using mip;

display i, E,D, X.l;

ShortestPath.PNG